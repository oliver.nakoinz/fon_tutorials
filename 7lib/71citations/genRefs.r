library(bibtex)
library("wrapr")  # get qc() definition

write.bib(qc(knitr,
             bibtex,
             wrapr,
             base,
             readr,
             dplyr,
             tidyr,
             ggplot2,
             tibble,
             purrr,
             magrittr,
             stringr,
             sqldf), file='7lib/71citations/lit_packages')


