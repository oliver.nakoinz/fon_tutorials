# Korrekturschritte für die Keramik2-Daten


## Fehlerliste 

1. Lesen ohne Faktoren  erledigt. 
2. Fehler in der Grabbezeichnung finden und beheben
3. Fehler in den Meßwerten finden und korrigieren
4. Fehler in den Spaltennamen finden und beheben   teilweise erledigt
5. key value in höhe und breite umwandeln
6. Zeilenduplikate, die durch key value verursacht werden entfernen
7. Beifunde in eine Spalte zusammenführen
8. Motive in mehrere Spalten aufteilen 
9. Die Motivspalten in 0/1 oder true false als Werte und nicht als Text
10. fehlende Koordinaten anhand der gefunden mit Werte nachtragen
11. Beifunde anhand der gefunde mit Werte vervollständigen
12. Variablen für die Anzahl der sonstigen Flaschen und Flachgefäße (Schalen und Schüsseln) einführen
13. Flachboden und flacher Boden zusammenführen
14. Grab und Brandgrab zusammenführen
15. Brandgraab löschen
16. Doppelte Zeile löschen
17. xyz löschen

## Workflow

1. Lesen ohne Faktoren
2. Fehler in Bezeichnungen finden und korrigieren
3. Spalten aufteilen
    - key value in höhe und breite umwandeln
    - Motive in mehrere Spalten aufteilen 
4. Spalten zusammenführen
    - Beifunde in eine Spalte zusammenführen
    - Grab und Brandgrab zusammenführen
5. Spalten löschen
    - redundante Spalten: Brandgraab
    - unnütze Spalten: xyz
6. Redundante Zeilen löschen
    - Zeilenduplikate, die durch key value verursacht werden entfernen
    - Doppelte Zeile löschen
7. Formate überprüfen
    - Text
    - Numerisch
        - Integer
        - Gleitkomma
    - Boolesche Werte
        - Die Motivspalten in 0/1 oder true false als Werte und nicht als Text
    - Faktoren
8. Werte ergänzen
    - fehlende Koordinaten anhand der gefunden mit Werte nachtragen
    - Beifunde anhand der gefunde mit Werte vervollständigen
9. Werte überprüfen
    - Fehler in der Grabbezeichnung finden und beheben: unique
    - Fehler in den Meßwerten finden und korrigieren: summary
    - unbekannt etc zu NA
    - Flachboden und flacher Boden zusammenführen
10. Neue Spalten einführen
    - Variablen für die Anzahl der sonstigen Flaschen und Flachgefäße (Schalen und Schüsseln) einführen


## Weitere Aufbereitung


### Attributbasierte Analyse

1. aufteilung der kategorialen variable in mehrere Attribute
2. Gewünschtes Format herstellen (1/0 oder Boolesch)


### Sondertabellen

1. id breite, hoehe, Motive extrahieren
2. Aufteilen in flach und hochformen
3. Normalisieren der Werte für eine weitere Tabelle














xxxxxxxxx Datensatzergänzungen
- kategoriale Variable mit mehreren Ausprägungen
