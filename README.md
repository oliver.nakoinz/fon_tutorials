# fon_tutorials

This repository contains tutorials for teaching and self teaching of quantitative archaeology. Most of the tutorials deal with R or SQL. Different natural languages as well as programming languages are considered. The content of the tutorials concerns basics, network analysis, cultural distance approach, data base techniques and spatial analysis. The didactic concept of the tutorials was developed by a Perle funded project while the content is based on different courses at the institute of pre- and protohistory of university of Kiel university, the Johanna Mestorf Academy (JMA) and ISAAK. This repository includes JMA and ISAAK tutorials which also are hosted at other (open or closed) repositories and is open for any contributions fitting the topic and concept.

The tutorials are made with `rmarkdown`. `tutorial_manual_v01.rmd` is a tutorial making this kind of tutorials. `tutorials.css` defines the html style of the tutorials. While all tutorials can be found in one directory (5documents), they are organized in a kind of hierarchical order concerning the content:

- Basics and Introductions
    - spat_intro_de_v04
    - sql_intro_en_v01
- Networks and Groups
    - sna_ceramics_en_v06.Rmd
    - culture_and_networks_en_v01.rmd
- Tutorial
    - spat_intro_v01.Rmd

The tutorials can be realized in different natural languages. A tag in the upper part of the tutorials indicates programming language, natural language and other details. We present the tutorials with the .rmd source code and a html file. 


